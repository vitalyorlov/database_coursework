<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlacklistUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $usersCount = DB::table('users')->count();

        for ($i = 0; $i < 10; $i++) {
            DB::table('blacklist_user')->insert([
                'user_id' => $faker->numberBetween(1, $usersCount)
            ]);
        }
    }
}
