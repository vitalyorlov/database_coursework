<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AboutPageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

        DB::table('about_page')->insert([
            'title' => 'About us',
            'description' => $faker->randomLetter,
            'contacts' => $faker->phoneNumber,
        ]);
    }
}
