<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConstantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $languagesCount = DB::table('language')->count();

        for ($i = 0; $i < 100; $i++) {
            DB::table('constant')->insert([
                'name' => $faker->word,
                'translate' => $faker->text(200),
                'language_id' => $faker->numberBetween(1, $languagesCount)
            ]);
        }
    }
}
