<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatisticValueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $reportCount = DB::table('report')->count();
        $statisticKeysCount = DB::table('statistic_key')->count();

        for($i = 0; $i < 10; $i++) {
            DB::table('statistic_value')->insert([
                'report_id' => $faker->numberBetween(1, $reportCount),
                'statistic_key_id' => $faker->numberBetween(1, $statisticKeysCount),
                'value' => $faker->word,
            ]);
        }
    }
}
