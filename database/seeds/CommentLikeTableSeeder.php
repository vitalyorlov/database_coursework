<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommentLikeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $commentsCount = DB::table('comment')->count();
        $usersCount = DB::table('users')->count();

        for ($i = 0; $i < 100; $i++) {
            DB::table('like_for_comment')->insert([
                'user_id' => $faker->numberBetween(1, $usersCount),
                'comment_id' => $faker->numberBetween(1, $commentsCount)
            ]);
        }
    }
}
