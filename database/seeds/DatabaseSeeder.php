<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TransactionStatusTableSeeder::class);
        $this->call(PaymentStatusTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(ItemStatusTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(MediaTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(CurrencyTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(ItemTableSeeder::class);
        $this->call(PersonalInfoTableSeeder::class);
        $this->call(TransactionTableSeeder::class);
        $this->call(AccountTableSeeder::class);
        $this->call(KeywordTableSeeder::class);
        $this->call(FaqCategoryTableSeeder::class);
        $this->call(FaqPageTableSeeder::class);
        $this->call(CommentTableSeeder::class);
        $this->call(LikeTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(FavouriteItemTableSeeder::class);
        $this->call(AboutPageTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(PaymentTableSeeder::class);
        $this->call(ResetPasswordTableSeeder::class);
        $this->call(LanguageTableSeeder::class);
        $this->call(CurrencyRateTableSeeder::class);
        $this->call(MetricTableSeeder::class);
        $this->call(ConstantTableSeeder::class);
        $this->call(DeliverPointTableSeeder::class);
        $this->call(DeliverOrderTableSeeder::class);
        $this->call(AttributeTypeTableSeeder::class);
        $this->call(AttributeTableSeeder::class);
        $this->call(PossibleAttributeValueTableSeeder::class);
        $this->call(AttributeValueTableSeeder::class);
        $this->call(UserActivationTableSeeder::class);
        $this->call(MessageTableSeeder::class);
        $this->call(SliderItemTableSeeder::class);
        $this->call(SaleTableSeeder::class);
        $this->call(ReportTypeTableSeeder::class);
        $this->call(UserActivationTableSeeder::class);
        $this->call(StatisticKeyTableSeeder::class);
        $this->call(ReportTableSeeder::class);
        $this->call(StatisticValueTableSeeder::class);
        $this->call(BlacklistItemTableSeeder::class);
        $this->call(BlacklistUserTableSeeder::class);
        $this->call(ItemToKeywordSeeder::class);
        $this->call(ItemToMediaSeeder::class);
        $this->call(RoleToPermissionSeeder::class);
    }
}
