<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrencyRateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $currenciesCount = DB::table('currency')->count();

        for ($i = 0; $i < 10; $i++) {
            DB::table('currency_rate')->insert([
                'cur_id_1' => $faker->numberBetween(1, $currenciesCount),
                'cur_id_2' => $faker->numberBetween(1, $currenciesCount),
                'rate' => $faker->randomFloat(0, 1)
            ]);
        }
    }
}
