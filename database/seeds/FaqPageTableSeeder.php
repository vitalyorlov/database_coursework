<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FaqPageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $categoriesCount = DB::table('faq_category')->count();

        for($i = 0; $i < 10; $i++) {
            DB::table('faq_page')->insert([
                'title' => $faker->word,
                'category_id' => $faker->numberBetween(1, $categoriesCount),
                'html' => $faker->randomLetter
            ]);
        }
    }
}
