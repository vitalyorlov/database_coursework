<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeliverOrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $transactionsCount = DB::table('transaction')->count();
        $deliverPointsCount = DB::table('deliver_point')->count();

        for($i = 0; $i < 10; $i++) {
            DB::table('deliver_order')->insert([
                'price' => $faker->randomFloat(0, 10000),
                'transaction_id' => $faker->numberBetween(1, $transactionsCount),
                'from_point_id' => $faker->numberBetween(1, $deliverPointsCount),
                'to_point_id' => $faker->numberBetween(1, $deliverPointsCount)
            ]);
        }
    }
}
