<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

        for($i = 0; $i < 100; $i++) {
            DB::table('permission')->insert([
                'name' => $faker->word,
                'description' => $faker->text(50)
            ]);
        }
    }
}
