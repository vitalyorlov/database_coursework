<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

        for ($i = 0; $i < 5; $i++) {
            DB::table('category')->insert([
                'name' => $faker->randomLetter,
                'parent_id' => null,
            ]);
        }

        for ($i = 0; $i < 200; $i++) {
            DB::table('category')->insert([
                'name' => $faker->word,
                'parent_id' => $faker->numberBetween(1, 5),
            ]);
        }
    }
}
