<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ResetPasswordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

        for ($i = 0; $i < 100; $i++) {
            DB::table('password_resets')->insert([
                'email' => $faker->email,
                'token' => $faker->sha256,
                'created_at' => $faker->dateTime
            ]);
        }
    }
}
