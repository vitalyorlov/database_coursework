<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SaleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $itemsCount = DB::table('item')->count();

        for ($i = 0; $i < 10; $i++) {
            DB::table('sale')->insert([
                'item_id' => $faker->numberBetween(1, $itemsCount),
                'percent' => $faker->randomFloat(0, 1),
            ]);
        }
    }
}
