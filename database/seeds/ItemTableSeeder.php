<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $usersCount = DB::table('users')->count();
        $categoriesCount = DB::table('category')->count();
        $statusesCount = DB::table('status_for_item')->count();
        $currenciesCount = DB::table('currency')->count();

        for($i = 0; $i < 1000; $i++) {
            DB::table('item')->insert([
                'name' => $faker->word,
                'description' => $faker->text(200),
                'price' => $faker->randomFloat(1, 10000),
                'user_id' => $faker->numberBetween(1, $usersCount),
                'category_id' => $faker->numberBetween(1, $categoriesCount),
                'status_id' => $faker->numberBetween(1, $statusesCount),
                'currency_id' => $faker->numberBetween(1, $currenciesCount),
            ]);
        }
    }
}
