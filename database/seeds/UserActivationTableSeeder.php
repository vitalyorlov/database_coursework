<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserActivationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $usersCount = DB::table('users')->count();

        for($i = 0; $i < 100; $i++) {
            DB::table('user_activation')->insert([
                'user_id' => $faker->numberBetween(1, $usersCount),
                'token' => $faker->sha256,
                'created_at' => $faker->dateTime
            ]);
        }
    }
}
