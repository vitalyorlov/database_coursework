<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleToPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $rolesCount = DB::table('roles')->count();
        $permissionsCount = DB::table('permission')->count();

        for($i = 0; $i < 10; $i++) {
            DB::table('role_to_permission')->insert([
                'role_id' => $faker->numberBetween(1, $rolesCount),
                'permission_id' => $faker->numberBetween(1, $permissionsCount)
            ]);
        }
    }
}
