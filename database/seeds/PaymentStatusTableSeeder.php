<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_for_payment')->insert([
            'name' => 'Started',
        ]);
        DB::table('status_for_payment')->insert([
            'name' => 'In Process',
        ]);
        DB::table('status_for_payment')->insert([
            'name' => 'Closed',
        ]);
        DB::table('status_for_payment')->insert([
            'name' => 'Cancelled',
        ]);
        DB::table('status_for_payment')->insert([
            'name' => 'Error',
        ]);
    }
}
