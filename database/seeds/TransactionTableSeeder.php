<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $usersCount = DB::table('users')->count();
        $itemsCount = DB::table('item')->count();
        $statusCount = DB::table('status_for_transaction')->count();

        for($i = 0; $i < 100; $i++) {
            DB::table('transaction')->insert([
                'item_id' => $faker->numberBetween(1, $itemsCount),
                'buyer_id' => $faker->numberBetween(1, $usersCount),
                'status_id' => $faker->numberBetween(1, $statusCount),
            ]);
        }
    }
}
