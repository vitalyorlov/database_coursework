<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemsTopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $itemsCount = DB::table('item')->count();

        for($i = 0; $i < 100; $i++) {
            DB::table('items_top')->insert([
                'item_id' => $faker->numberBetween(1, $itemsCount),
                'description' => $faker->text(200),
                'date_add' => $faker->dateTime,
                'date_end' => $faker->dateTime
            ]);
        }
    }
}
