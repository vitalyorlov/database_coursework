<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AttributeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $typesCount = DB::table('attribute_type')->count();
        $categoriesCount = DB::table('category')->count();

        for($i = 0; $i < 10; $i++) {
            DB::table('attribute_for_category')->insert([
                'name' => $faker->word,
                'type_id' => $faker->numberBetween(1, $typesCount),
                'category_id' => $faker->numberBetween(1, $categoriesCount),
            ]);
        }
    }
}
