<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PersonalInfoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $usersCount = DB::table('users')->count();
        $citiesCount = DB::table('city')->count();

        for($i = 0; $i < 100; $i++) {
            DB::table('personal_info')->insert([
                'user_id' => $i + 1,
                'phone_number' => $faker->phoneNumber,
                'city_id' => $faker->numberBetween(1, $citiesCount),
                'address' => $faker->address,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'birth_date' => $faker->dateTime,
            ]);
        }
    }
}
