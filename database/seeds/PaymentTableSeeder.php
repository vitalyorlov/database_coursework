<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $accountsCount = DB::table('account')->count();
        $transactionsCount = DB::table('transaction')->count();
        $statusCount = DB::table('status_for_payment')->count();

        for($i = 0; $i < 100; $i++) {
            DB::table('payment')->insert([
                'buyer_account_id' => $faker->numberBetween(1, $accountsCount),
                'seller_account_id' => $faker->numberBetween(1, $accountsCount),
                'sum' => $faker->randomFloat(0, 100),
                'transaction_id' => $faker->numberBetween(1, $transactionsCount),
                'status_id' => $faker->numberBetween(1, $statusCount),
            ]);
        }
    }
}
