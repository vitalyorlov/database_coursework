<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FaqCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

        for ($i = 0; $i < 5; $i++) {
            DB::table('faq_category')->insert([
                'name' => $faker->randomLetter,
                'parent_id' => null,
            ]);
        }

        for ($i = 0; $i < 100; $i++) {
            DB::table('faq_category')->insert([
                'name' => $faker->word,
                'parent_id' => $faker->numberBetween(1, 5),
            ]);
        }
    }
}
