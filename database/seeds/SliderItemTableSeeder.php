<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SliderItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $itemsCount = DB::table('item')->count();
        $imagesCount = DB::table('media')->count();

        for ($i = 0; $i < 10; $i++) {
            DB::table('slider_item')->insert([
                'text' => $faker->text(200),
                'item_id' => $faker->numberBetween(1, $itemsCount),
                'image_id' => $faker->numberBetween(1, $imagesCount),
                'date_add' => $faker->dateTime,
                'date_end' => $faker->dateTime
            ]);
        }
    }
}
