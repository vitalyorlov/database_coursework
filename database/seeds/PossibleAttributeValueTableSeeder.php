<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PossibleAttributeValueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $attributesCount = DB::table('attribute_for_category')->count();

        for($i = 0; $i < 100; $i++) {
            DB::table('possible_values_for_attr')->insert([
                'name' => $faker->word,
                'attribute_for_category_id' => $faker->numberBetween(1, $attributesCount)
            ]);
        }
    }
}
