<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReportTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $typesCount = DB::table('report_type')->count();

        for($i = 0; $i < 10; $i++) {
            DB::table('report')->insert([
                'name' => $faker->word,
                'type_id' => $faker->numberBetween(1, $typesCount),
                'date' => $faker->dateTime,
            ]);
        }
    }
}
