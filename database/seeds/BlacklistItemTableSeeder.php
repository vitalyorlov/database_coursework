<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlacklistItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $itemsCount = DB::table('item')->count();

        for ($i = 0; $i < 10; $i++) {
            DB::table('blacklist_item')->insert([
                'item_id' => $faker->numberBetween(1, $itemsCount)
            ]);
        }
    }
}
