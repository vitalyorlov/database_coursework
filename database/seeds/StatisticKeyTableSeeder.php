<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatisticKeyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $reportTypesCount = DB::table('report_type')->count();

        for($i = 0; $i < 10; $i++) {
            DB::table('statistic_key')->insert([
                'name' => $faker->word,
                'query' => $faker->word,
                'report_type_id' => $faker->numberBetween(1, $reportTypesCount),
            ]);
        }
    }
}
