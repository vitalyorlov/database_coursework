<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_for_item')->insert([
            'name' => 'Ready for sale',
        ]);
        DB::table('status_for_item')->insert([
            'name' => 'Banned by admin',
        ]);
        DB::table('status_for_item')->insert([
            'name' => 'Temporary not for sale',
        ]);
        DB::table('status_for_item')->insert([
            'name' => 'In sale process',
        ]);
    }
}
