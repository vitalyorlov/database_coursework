<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $rolesCount = DB::table('roles')->count();

        for($i = 0; $i < 100; $i++) {
            DB::table('users')->insert([
                'login' => $faker->word,
                'email' => $faker->email,
                'password' => $faker->password,
                'activated' => $faker->boolean,
                'role_id' => $faker->numberBetween(1, $rolesCount),
            ]);
        }

    }
}
