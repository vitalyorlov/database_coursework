<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AttributeValueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $itemsCount = DB::table('item')->count();
        $attributesCount = DB::table('attribute_for_category')->count();

        for($i = 0; $i < 10; $i++) {
            DB::table('attribute_value')->insert([
                'value' => $faker->word,
                'attribute_for_category_id' => $faker->numberBetween(1, $attributesCount),
                'item_id' => $faker->numberBetween(1, $itemsCount),
            ]);
        }
    }
}
