<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemToMediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $mediasCount = DB::table('media')->count();
        $itemsCount = DB::table('item')->count();

        for($i = 0; $i < 10; $i++) {
            DB::table('item_to_media')->insert([
                'item_id' => $faker->numberBetween(1, $itemsCount),
                'media_id' => $faker->numberBetween(1, $mediasCount)
            ]);
        }
    }
}
