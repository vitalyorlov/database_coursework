<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReportTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

        for ($i = 0; $i < 10; $i++) {
            DB::table('report_type')->insert([
                'name' => $faker->word
            ]);
        }
    }
}
