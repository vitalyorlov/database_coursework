<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MetricTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

        DB::table('metric')->insert([
            'name' => 'temperature',
            'value' => 'Celsius',
            'language_id' => 1
        ]);

        DB::table('metric')->insert([
            'name' => 'temperature',
            'value' => 'Farengate',
            'language_id' => 2
        ]);
    }
}
