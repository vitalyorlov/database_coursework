<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $countriesCount = DB::table('country')->count();

        for ($i = 0; $i < 10; $i++) {
            DB::table('currency')->insert([
                'name' => $faker->currencyCode,
                'abbr' => $faker->currencyCode,
                'country_id' => $faker->numberBetween(1, $countriesCount)
            ]);
        }
    }
}
