<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

        for ($i = 0; $i < 100; $i++) {
            DB::table('country')->insert([
                'name' => $faker->country,
                'abbr' => $faker->countryCode
            ]);
        }
    }
}
