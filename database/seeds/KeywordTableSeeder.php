<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KeywordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

        for ($i = 0; $i < 1000; $i++) {
            DB::table('keyword')->insert([
                'name' => $faker->word
            ]);
        }
    }
}
