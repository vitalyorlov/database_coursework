<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeliverPointTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $citiesCount = DB::table('city')->count();

        for($i = 0; $i < 10; $i++) {
            DB::table('deliver_point')->insert([
                'address' => $faker->address,
                'city_id' => $faker->numberBetween(1, $citiesCount)
            ]);
        }
    }
}
