<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $usersCount = DB::table('users')->count();

        for($i = 0; $i < 100; $i++) {
            DB::table('message')->insert([
                'from_user_id' => $faker->numberBetween(1, $usersCount),
                'to_user_id' => $faker->numberBetween(1, $usersCount),
                'text' => $faker->text(50),
                'date' => $faker->dateTime
            ]);
        }
    }
}
