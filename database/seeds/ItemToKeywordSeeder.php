<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemToKeywordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $keywordsCount = DB::table('keyword')->count();
        $itemsCount = DB::table('item')->count();

        for($i = 0; $i < 10; $i++) {
            DB::table('item_to_keyword')->insert([
                'item_id' => $faker->numberBetween(1, $itemsCount),
                'keyword_id' => $faker->numberBetween(1, $keywordsCount)
            ]);
        }
    }
}
