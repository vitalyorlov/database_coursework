<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LikeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $itemsCount = DB::table('item')->count();
        $usersCount = DB::table('users')->count();

        for($i = 0; $i < 100; $i++) {
            DB::table('like')->insert([
                'item_id' => $faker->numberBetween(1, $itemsCount),
                'user_id' => $faker->numberBetween(1, $usersCount)
            ]);
        }
    }
}
