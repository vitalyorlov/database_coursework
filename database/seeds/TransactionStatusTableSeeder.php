<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactionStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

        DB::table('status_for_transaction')->insert([
            'name' => 'Started',
        ]);
        DB::table('status_for_transaction')->insert([
            'name' => 'In Process',
        ]);
        DB::table('status_for_transaction')->insert([
            'name' => 'Closed',
        ]);
        DB::table('status_for_transaction')->insert([
            'name' => 'Banned by admin',
        ]);
        DB::table('status_for_transaction')->insert([
            'name' => 'Canceled',
        ]);
    }
}
