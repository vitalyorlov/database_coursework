<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $usersCount = DB::table('users')->count();

        for ($i = 0; $i < 1000; $i++) {
            DB::table('account')->insert([
                'balance' => $faker->randomFloat(0, 10000),
                'account_number' => $faker->bankAccountNumber,
                'user_id' => $faker->numberBetween(1, $usersCount),
                'password' => $faker->password(1, 60)
            ]);
        }
    }
}
