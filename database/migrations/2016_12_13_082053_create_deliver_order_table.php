<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliverOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliver_order', function (Blueprint $table) {
            $table->increments('id');
            $table->double('price');
            $table->integer('from_point_id')->unsigned();
            $table->integer('to_point_id')->unsigned();
            $table->integer('transaction_id')->unsigned()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliver_order');
    }
}
