<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationsForTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::table('user', function ($table)
//        {
//            $table->foreign('role_id')->references('id')->on('role')->onDelete('cascade');
//        });

        Schema::table('item_to_media', function ($table)
        {
            $table->foreign('item_id')->references('id')->on('item')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('media_id')->references('id')->on('media')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('city', function ($table)
        {
            $table->foreign('country_id')->references('id')->on('country')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('category', function ($table)
        {
            $table->foreign('parent_id')->references('id')->on('category')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('item', function ($table)
        {
            $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('status_id')->references('id')->on('status_for_item')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('currency_id')->references('id')->on('currency')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('personal_info', function ($table)
        {
            $table->foreign('city_id')->references('id')->on('city')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('transaction', function ($table)
        {
            $table->foreign('item_id')->references('id')->on('item')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('buyer_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('status_id')->references('id')->on('status_for_transaction')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('item_to_keyword', function ($table)
        {
            $table->foreign('item_id')->references('id')->on('item')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('keyword_id')->references('id')->on('keyword')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('comment', function ($table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('item_id')->references('id')->on('item')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('like', function ($table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('item_id')->references('id')->on('item')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('faq_category', function ($table)
        {
            $table->foreign('parent_id')->references('id')->on('faq_category')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('role_to_permission', function ($table)
        {
            $table->foreign('permission_id')->references('id')->on('permission')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('favourite_item', function ($table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('item_id')->references('id')->on('item')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('payment', function ($table)
        {
            $table->foreign('buyer_account_id')->references('id')->on('account')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('seller_account_id')->references('id')->on('account')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('transaction_id')->references('id')->on('transaction')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('status_id')->references('id')->on('status_for_payment')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('currency', function ($table)
        {
            $table->foreign('country_id')->references('id')->on('country')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('language', function ($table)
        {
            $table->foreign('country_id')->references('id')->on('country')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('currency_rate', function ($table)
        {
            $table->foreign('cur_id_1')->references('id')->on('currency')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('cur_id_2')->references('id')->on('currency')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('metric', function ($table)
        {
            $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('like_for_comment', function ($table)
        {
            $table->foreign('comment_id')->references('id')->on('comment')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('constant', function ($table)
        {
            $table->foreign('language_id')->references('id')->on('language')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('deliver_order', function ($table)
        {
            $table->foreign('transaction_id')->references('id')->on('transaction')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('from_point_id')->references('id')->on('deliver_point')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('to_point_id')->references('id')->on('deliver_point')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('deliver_point', function ($table)
        {
            $table->foreign('city_id')->references('id')->on('city')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('attribute_for_category', function ($table)
        {
            $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('type_id')->references('id')->on('attribute_type')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('attribute_value', function ($table)
        {
            $table->foreign('attribute_for_category_id')->references('id')->on('attribute_for_category')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('item_id')->references('id')->on('item')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('possible_values_for_attr', function ($table)
        {
            $table->foreign('attribute_for_category_id')->references('id')->on('attribute_for_category')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('message', function ($table)
        {
            $table->foreign('from_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('to_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('slider_item', function ($table)
        {
            $table->foreign('image_id')->references('id')->on('media')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('item_id')->references('id')->on('item')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('items_top', function ($table)
        {
            $table->foreign('item_id')->references('id')->on('item')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('statistic_key', function ($table)
        {
            $table->foreign('report_type_id')->references('id')->on('report_type')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('report', function ($table)
        {
            $table->foreign('type_id')->references('id')->on('report_type')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('statistic_value', function ($table)
        {
            $table->foreign('report_id')->references('id')->on('report')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('statistic_key_id')->references('id')->on('statistic_key')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('blacklist_item', function ($table)
        {
            $table->foreign('item_id')->references('id')->on('item')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('blacklist_user', function ($table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
