@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if(Auth::guest())
                    <a href="/login" class="btn btn-info"> You need to login to see your name</a>
                @endif
            </div>
        </div>
    </div>
@endsection