<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <script src="{{ URL::asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ url('/') }}">Trading platform</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a class="nav-item nav-link active" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a></li>
                    <li><a class="nav-item nav-link" href="#">Features</a></li>
                    <li><a class="nav-item nav-link" href="#">Pricing</a></li>
                    <li><a class="nav-item nav-link" href="#">About</a></li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                    @if (Auth::guest())
                        <li><a class="nav-item nav-link" href="{{ url('/login') }}">Login</a></li>
                        <li><a class="nav-item nav-link" href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->login }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @yield('content')
                </div>
            </div>
        </div>
        <nav class="navbar navbar-fixed-bottom navbar-light bg-faded">
            <span class="navbar-text float-xs-left">Developed by Vitaly Orlov © 2016</span>
        </nav>
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
</body>
</html>
